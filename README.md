# rfiglet
figletでフォントをランダムに選択するもの。

## Require
* ruby
* figlet

## Install
### UbuntuなどDebian系の場合
1. `sudo apt install figlet`
2. `sudo make install`

## Useage
`rfigret <text>`

## 主な用途
1. `sudo cp 0-hostname /etc/update-motd.d/`とすると、ログイン時にホスト名が大きく表示されるようになります。
2. `sudo apt install lolcat`すると、さらにカラフルになります。


sanadan <jecy00@gmail.com>

